<?php
    class Product {
        public $name;
        public $price;
        public $description;
        public $category;
        public $stockNo;  

        public function __construct($nameValue, $priceValue, $descriptionValue, $categoryValue, $stockNo) {

			$this->name = $nameValue;
			$this->price = $priceValue;
			$this->description = $descriptionValue;
			$this->category = $categoryValue;
			$this->stockNo = $stockNoValue;
		}


        public function printDetails(){
            return  "The product has a name of $this->name and its price is $this->price, and the stock no is $this->stockNo." ;
		}

		public function getName(){ 
			return $this->name;
		}
		public function getPrice(){
			return $this->price;
		}
		public function getDescription(){
			return $this->description;
		}
		public function getCategory(){
			return $this->category;
		}
		public function getstockNo(){
			return $this->stockNo;
		}
	
		public function setName($nameValue){ 
			$this->name = $nameValue;
		}
		public function setPrice($priceValue){
			$this->price = $priceValue;
		}
		public function setDescription($descriptionValue){
			$this->description = $descriptionValue;
		}
		public function setCategory($categoryValue){
			$this->category = $categoryValue;
		}
		public function setstockNo($stockNoValue){
			$this->stockNo= $stockNoValue;
		}

	}

	$newProduct = new Product('Xioami Mi Monitor', 22000.00, 'Good for gaming and for coding', 'computer-peripherals', 5);


	class Mobile extends Product { 
		public function printDetails() { //Our latest mobile is <mobile name> with a cheapest price of <mobileprice>
			return "Our latest mobile is name $this->name with a cheapest price of price $this->price.";
		}
	}

	$newMobile = new Mobile('Xioami Redmi Note 10 pro', 13590.00, 'Latest Xioami phone ever made', 'mobiles and electronics', 10);


	class Computer extends Product { 
		public function printDetails() { //Our newest computer is <computer name> and its base price is <computer price>
			return "Our newest computer is $this->name and its base price is $this->price.";
		}
	}

	$newComputer = new Computer('HP Business Laptop', 29000.00, 'HP Laptop only made for business', 'laptops and computer', '10');

 ?>

        