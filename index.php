<?php require "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<title>PHP OOP | Activity</title>
</head>
<body>
	<ul>
       <li><?php echo $newProduct->printDetails(); ?></li>
		<li><?php echo $newMobile->printDetails(); ?></li>
		<li><?php echo $newComputer->printDetails(); ?></li>

    </ul>


	<?php $newProduct->setstockNo(3); ?>
    <?php $newMobile->setstockNo(5); ?>
    <?php $newComputer->setCategory("laptops, computers and electronics"); ?>

    <ul>
    	<li>
    		Updated stock number of product object: <?php echo $newProduct->getstockNo(); ?>
    	</li>
    	<li>
    		Updated stock number of mobile object: <?php echo $newMobile->getstockNo(); ?>
    	</li>
    	<li>
    		Updated category of computer object: <?php echo $newComputer->getCategory(); ?>
    	</li>
    </ul>


</body>
</html>

